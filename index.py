import tornado.web
import tornado.ioloop

class basic(tornado.web.RequestHandler):
    def get(self):
        self.write("hell,word!!!")

class static(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")
        
# class resoruce(tornado.web.RequestHandler):
#     def get(self,id):
#         self.write("query with id:"+ id)
        
class query(tornado.web.RequestHandler):
    def get(self):
        n = int(self.get_argument("n"))
        r= "odd" if n % 2 else "even"
        self.write("the number"+str(n)+" is " + r)
        
if __name__=="__main__":
    app=tornado.web.Application([
        (r"/hello",basic),
        (r"/",static),
        (r"/iseven",query),
        #have error to run becuse params not find
        # (r"/blog/[0-9] +)",resoruce)
    ])
    app.listen(8080)
    print("Iam listing on port 8080")
    tornado.ioloop.IOLoop.current().start()
